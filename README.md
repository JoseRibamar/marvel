# My Heroes

#### Objecive:
Develop Marvel App, consuming marvel's api.

#### Project using:
- MVVM Architecture
- Kotlin
- ViewModel & LiveData
- Coroutines
- Room
- Koin
- Retrofit & Gson
- Moshi
- Glide
- Lottie
- Mockito
- Espresso
- CI/CD GitLab
- SwipeRefreshLayout


#### Code Features:
- Suspend functions with Coroutines to HTTP requests
- Coroutines Flow with Room to listener changes from local database
- Shared ViewModel between 2 fragments ( and injected with Koin )
- Room TypeConverters to store complex data easily
- Dependecy Injection Tests
- Accessibility
- Internationalization
