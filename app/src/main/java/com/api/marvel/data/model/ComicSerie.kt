package com.api.marvel.data.model

data class ComicSerie (val name : String, var imageUrl : String)