package com.api.marvel.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.api.marvel.data.model.Character
import com.api.marvel.util.Converters

@Database(entities = [Character::class], version = 6)
@TypeConverters(
    Converters.SeriesConverter::class,
    Converters.ComicsConverter::class,
    Converters.StoriesConverter::class,
    Converters.EventsConverter::class,
    Converters.ThumbnailConverter::class
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun characterDao(): CharacterDao
}