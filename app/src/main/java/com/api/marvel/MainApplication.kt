package com.api.marvel

import android.app.Application
import com.api.marvel.di.module.AppModule
import com.api.marvel.di.module.NetworkModule
import com.api.marvel.di.module.dbModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@MainApplication)
            modules(listOf(AppModule,NetworkModule, dbModule))
        }

    }
}