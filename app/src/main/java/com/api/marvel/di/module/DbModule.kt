package com.api.marvel.di.module

import androidx.room.Room
import com.api.marvel.data.source.local.AppDatabase
import org.koin.dsl.module

val dbModule = module {
    single {
        Room.databaseBuilder(
            get(),
            AppDatabase::class.java,
            "marvel-database"
        ).fallbackToDestructiveMigration()
            .build()
    }
    factory { get<AppDatabase>().characterDao() }
}
