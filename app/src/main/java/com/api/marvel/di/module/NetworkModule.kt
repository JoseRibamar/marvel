package com.api.marvel.di.module

import com.api.marvel.BuildConfig
import com.api.marvel.data.repository.MarvelRepositoryImp
import com.api.marvel.data.source.local.CharacterDao
import com.api.marvel.data.source.remote.ApiService
import com.api.marvel.domain.repository.MarvelRepository
import com.api.marvel.util.Constants
import com.api.marvel.util.HashGenerate
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val NetworkModule = module {

    single { createService(get()) }

    single { createRetrofit(get(), BuildConfig.BASE_URL) }

    single { createOkHttpClient() }
}

fun createOkHttpClient(): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
    return OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .addInterceptor { chain -> createParametersDefault(chain) }
        .addInterceptor(httpLoggingInterceptor).build()

}

fun createRetrofit(okHttpClient: OkHttpClient, url: String): Retrofit {
    return Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun createParametersDefault(chain: Interceptor.Chain): Response {
    val timeStamp = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())
    var request = chain.request()
    val builder = request.url().newBuilder()

    builder.addQueryParameter(Constants.API_MARVEL_APIKEY, BuildConfig.API_PUBLIC)
        .addQueryParameter(Constants.API_MARVEL_HASH, HashGenerate.generate(timeStamp, BuildConfig.API_PRIVATE, BuildConfig.API_PUBLIC))
        .addQueryParameter(Constants.API_MARVEL_TIMESTAMP, timeStamp.toString())

    request = request.newBuilder().url(builder.build()).build()
    return chain.proceed(request)
}

fun createService(retrofit: Retrofit): ApiService {
    return retrofit.create(ApiService::class.java)
}

fun createMarvelRepository(apiService: ApiService, characterDao: CharacterDao): MarvelRepository {
    return MarvelRepositoryImp(apiService,characterDao)
}
