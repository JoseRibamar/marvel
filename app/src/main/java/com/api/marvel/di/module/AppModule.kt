package com.api.marvel.di.module


import com.api.marvel.presentation.characters.fragments.CharactersAdapter
import com.api.marvel.presentation.characters.fragments.CharactersViewModel
import com.api.marvel.presentation.detail.DetailViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val AppModule = module {

    viewModel { CharactersViewModel(get()) }
    viewModel { DetailViewModel(get()) }

    factory { CharactersAdapter() }

    single { createMarvelRepository(get(), get()) }
}