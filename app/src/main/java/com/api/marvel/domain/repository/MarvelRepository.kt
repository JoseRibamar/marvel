package com.api.marvel.domain.repository


import com.api.marvel.data.model.CharactersResponse
import com.api.marvel.data.model.ComicsResponse
import com.api.marvel.data.model.SerieResponse
import kotlinx.coroutines.flow.Flow
import com.api.marvel.data.model.Character

interface MarvelRepository {

    //remote
    suspend fun getCharacters(limit: Int, offset: Int): CharactersResponse
    suspend fun getComicsByCharacterId(id: String): ComicsResponse
    suspend fun getSeriesByCharacterId(id: String): SerieResponse

    //local
    suspend fun saveCharacterLocal(character: Character): Long
    suspend fun getCharactersLocal(): Flow<MutableList<Character>>
    suspend fun deleteCharacterLocal(character: Character)
}