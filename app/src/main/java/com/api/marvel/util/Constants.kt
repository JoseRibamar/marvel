package com.api.marvel.util

class Constants{

    companion object {
        //Extras
        val API_MARVEL_CHARACTER = "character"

        //NetworkModule

        val API_MARVEL_APIKEY = "apikey"
        val API_MARVEL_HASH = "hash"
        val API_MARVEL_TIMESTAMP = "ts"


        //number of colluns
        val API_MARVEL_TWO_COLUMNS = 2
        val API_MARVEL_THREE_COLUMNS = 3
    }
}