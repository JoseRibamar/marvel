package com.api.marvel.presentation.characters.fragments

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.api.marvel.R
import com.api.marvel.presentation.detail.DetailActivity
import com.api.marvel.util.Constants
import com.api.marvel.util.invisible
import com.api.marvel.util.visible
import kotlinx.android.synthetic.main.favorite_fragment.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel

class FavoriteFragment : Fragment() {

    companion object {
        fun newInstance() =
            FavoriteFragment()
    }

    private val viewModel: CharactersViewModel by sharedViewModel()
    private val charactersAdapter: CharactersAdapter by inject()
    private lateinit var layoutManagerGrid: GridLayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.favorite_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupFavoriteRecyclerView()
        getFavorites()
        getListCharactersFavorites()
        deleteCharacter()
        goDetailActivity()
        swipeRefreshList()
    }

    private fun getFavorites() {
        viewModel.getFavorites()
    }

    private fun getListCharactersFavorites() {
        viewModel.listCharactersFavorites.observe(this, Observer { list ->
            when (list.isNullOrEmpty()) {
                true -> {
                    imageViewNoFavorite.visible()
                    textViewNoFavorite.visible()
                }
                false -> {
                    imageViewNoFavorite.invisible()
                    textViewNoFavorite.invisible()
                }
            }
            charactersAdapter.addCharacters(list)
        })
    }

    private fun deleteCharacter() {
        charactersAdapter.setOnUnFavoriteClickListener {
            viewModel.deleteCharacter(it)
        }
    }

    private fun goDetailActivity() {
        charactersAdapter.setOnClickCharacterListener { character ->
            startActivity(Intent(activity, DetailActivity::class.java).apply {
                putExtra(Constants.API_MARVEL_CHARACTER, character)
            })
        }
    }

    private fun setupFavoriteRecyclerView() {
        layoutManagerGrid =
            if (activity?.resources?.configuration?.orientation == Configuration.ORIENTATION_PORTRAIT) {
                GridLayoutManager(activity, Constants.API_MARVEL_TWO_COLUMNS)
            } else {
                GridLayoutManager(activity, Constants.API_MARVEL_THREE_COLUMNS)
            }
        favoriteRecyclerView.apply {
            adapter = charactersAdapter
            setHasFixedSize(true)
            layoutManager = layoutManagerGrid
        }
    }

    private fun swipeRefreshList() {
        swiperefreshFavorite.setOnRefreshListener {
            viewModel.getFavorites()
            swiperefreshFavorite.isRefreshing = false
        }
    }


}
